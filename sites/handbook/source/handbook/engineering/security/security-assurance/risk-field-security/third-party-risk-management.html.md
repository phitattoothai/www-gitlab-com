---
layout: markdown_page
title: "Third Party Risk Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

In order to minimize the risk associated with third party applications and services, the Risk and Field Security Team has designed this procedure to document the intake, assessment, and monitoring of Third Party Risk Management activities. 

## Scope 

The Third Party Risk Management procedure is applicable to any third party application or service being provided to GitLab. This includes, but is not limited to, third parties providing free or paid applications or services, professional services organizations, contractors, alliances or partnerships, and mergers and acquisitions. 

## Roles and Responsibilities

| Role | Responsibility |
| ------ | ------ |
| **Risk and Field Security team** | Maintain a mechanism to intake and respond to Third Party Risk Management Activities |
| | Provide complete and accurate responses within documented SLA |
| | Document and report any risks or trends identified during Third Party Risk Management Activities |
| | Maintain Metrics |
| **Business or System Owner** | Submit request to Risk and Field Security|
| | Work with the R&FS team to complete the assessment |
| | Accept or Remediate any observations identified |

## Third Party Risk Management Activities Workflow

Regardless of the type of Third Party being assessed, it is important to document the inherent risk they might pose to GitLab. In order to centralize this process, the Risk and Field Security will intake and assess all third parties based on a common methodology. It is important to note that the Third Party Risk Management procedure is meant to function **alongside** of GitLab's procurement and contracting processes and is not a **replacement** for any other required reviews and approvals. 

```mermaid
graph TD
subgraph Legend
	Link[These are clickable boxes]
	end
  POS[Purchase Request:<br>Software]-->1
  FRAP[Free Software] -->1
  POPS[Purchase Request:<br>Prof Serv/Contractors]-->1
  PMF[Purchase Request:<br>Field Mktg & Events]-->1
  ALLI[Alliances]-->1
  MRG[Mergers &<br>Acquisitions] -->1
  1[Submit Request] 
  1--> B{Inherent Risk<br>Determined}
  B -->|Low| D[No Further Action]
  B -->|Moderate| E[Assessment Required]
  B -->|High| F[Assessment and<br/>App Sec Review<br/>Required]

click POS "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-saas/"
style POS fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click POPS "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-professional-services/"
style POPS fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click PMF "https://about.gitlab.com/handbook/finance/procurement/vendor-contract-marketing/"
style PMF fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

click FRAP "https://about.gitlab.com/handbook/communication/#need-to-add-a-new-app-to-slack"
style FRAP fill:#EAF2FB, stroke:#2266AA, stroke-width:2px

style Link fill:#EAF2FB, stroke:#2266AA, stroke-width:2px
```

## Security Assessment 

If a Security Assessment is deemed as necessary, the Risk and Field Security team conducts the following steps:

1. Reviews independent audits such as SOC1 Type 2, SOC2 Type 2 or similar
1. Reviews external testing such as independent vulnerability scan reports or penetration test reports
1. Engages the Application Security team, as needed, to conducted a technical assessment
1. Documents control effectiveness and ability to meet Third Party Minimum Security Standards standard.
1. Documents the Residual Risk (the risk after assessing compensating controls)

All Third Party Risk Management activities are managed out of ZenGRC. 

<p>
<details>
<summary>Additional Information </summary>

Why do you need an independent audit report and external testing?

> Penetration testing and Vulnerability scans have a very different scope and goal than audits of an information security program, but they are very useful when assessing the maturity of a Third Party's information security program. The fact that a Third Party has completed such a test indicates that they are operating a mature security program and are reducing the likelihood of a vulnerability existing in architecture or software. 

What if a Third Party is providing a security whitepaper instead?

> Unfortunately, security whitepapers rarely give us all of the information we need. When a company generates these whitepapers they are always from the perspective of everything the Third Party feels like they are doing well, but won't have any information about what controls are not in place or not yet mature. For this reason we won't accept whitepapers in place of audit reports or the GitLab Information Security Questionnaire, but the Third Party can absolutely attach the whitepaper and reference any information in there when completing our questionnaire if that makes the process easier for the Third Party. Feel free to use this email template to request documentation:

Email Template: 
>Because the use of your tool or services requires the protection of GitLab data and/or there is a reliance on your tool or service to support various business/financial processes at GitLab, our Security team will need some additional information to ensure that controls in your environment provide reasonable assurance that processes we rely on are operating effectively and that our data is appropriately secured. If you have gone through an independent certification for a SOC2 Type 2 and have undergone a recent PenTest, please provide a copy of these reports or summary of results. Additionally, if there are bridge letters available to supplement your SOC reports, please provide these so that our Security team can determine that there have been no major changes to controls over your environment between certifications. If you have not gone through these certification processes, please complete the GitLab Information Security Questionnaire and answer only the applicable questions. We appreciate your partnership in helping GitLab complete its due diligence requirements.

</details>

## Service Level Agreements

Once the request is received, the Risk and Field Security will complete the activities within **10 business days**. 

## Exceptions

Not Applicable

## References
Third Party Minimum Security Standards
